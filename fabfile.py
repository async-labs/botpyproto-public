# coding: utf-8
from __future__ import (
    absolute_import, division, print_function, unicode_literals
)

from fabric.decorators import task
from fabric.operations import local


@task
def lint():
    local('flake8 --statistics')


@task
def install_requirements():
    local('pip install -r requirements.txt')


@task
def migrate():
    local('python manage.py migrate')


@task
def runserver():
    local('python manage.py runserver 127.0.0.1:8000')
