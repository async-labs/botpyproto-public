# Python telegram bot prototype
1. First of all, you have to register bot via telegram bot [@BotFather](https://t.me/botfather) and obtain token. It looks like this `123456789:ABCdef-GhIJKlmNoPQRsTUVwxyZ`.

2. Than you need to replace token in `TELEGRAM_TOKEN` variable in `botpyproto/settings.py` with the one you obtained.

3. Start vagrant. Make sure that 8000 port is free
```bash
vagrant up
```
4. Connect to vagrant via ssh
```bash
vagrant ssh
```
5. Check that all migrations are applied
```bash
fab migrate
```
6. Start the bot
```bash
fab runserver
```
7. Bot is available at [@pyprotobot](https://t.me/pyprotobot)
