#!/usr/bin/env bash
set -e

# Install requirements  for Python
apt-get update
apt-get install -y gcc make zlib1g-dev libssl-dev libmysqlclient-dev

# Make temporary dir
PYTHON_BUILD_DIR="/tmp/python"
mkdir -p $PYTHON_BUILD_DIR
cd $PYTHON_BUILD_DIR

# Download and extract Python source code
PYTHON_VERSION="2.7.13"
wget http://www.python.org/ftp/python/$PYTHON_VERSION/Python-$PYTHON_VERSION.tar.xz
tar -xvf Python-$PYTHON_VERSION.tar.xz

# Configure, build and install Python
cd Python-$PYTHON_VERSION
./configure --prefix=/usr/local
make
make install

# Add /usr/local/bin ahead of /usr/bin
export PATH="/usr/local/bin:$PATH"
echo 'export PATH="/usr/local/bin:$PATH"' >> /home/ubuntu/.bashrc

# mysql
debconf-set-selections <<< 'mysql-server mysql-server/root_password password root'
debconf-set-selections <<< 'mysql-server mysql-server/root_password_again password root'
apt-get -y install mysql-server
sed -i "s/^bind-address/#bind-address/" /etc/mysql/my.cnf
mysql -u root -proot -e "GRANT ALL PRIVILEGES ON *.* TO 'root'@'localhost' IDENTIFIED BY 'root' WITH GRANT OPTION; FLUSH PRIVILEGES; SET GLOBAL max_connect_errors=10000;"
/etc/init.d/mysql restart

# database init
mysql -u root -proot -e "CREATE DATABASE IF NOT EXISTS botpyproto CHARACTER SET UTF8;"
mysql -u root -proot -e "CREATE USER IF NOT EXISTS botpyproto IDENTIFIED BY 'botpyproto';"
mysql -u root -proot -e "GRANT ALL PRIVILEGES ON botpyproto.* TO botpyproto;"
mysql -u root -proot -e "FLUSH PRIVILEGES;"

# restart
/etc/init.d/mysql restart

# Pip
curl -sL https://bootstrap.pypa.io/get-pip.py | python

# Auto cd to sources dir
echo 'cd ~/botpyproto' >> /home/ubuntu/.bashrc

# Install deps for python backend
cd /home/ubuntu/botpyproto
pip install -r requirements.txt
