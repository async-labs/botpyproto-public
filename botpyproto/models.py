# coding: utf-8
from __future__ import (
    absolute_import, division, print_function, unicode_literals
)

from django.db import models


class TelegramUser(models.Model):
    username = models.CharField(max_length=50, blank=True)
    first_name = models.CharField(max_length=50, blank=True)
    last_name = models.CharField(max_length=50, blank=True)
    id = models.IntegerField(primary_key=True)
    refused = models.BooleanField(default=False)
