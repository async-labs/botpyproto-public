# coding: utf-8
from __future__ import (
    absolute_import, division, print_function, unicode_literals
)

from django.conf import settings
from telegram.ext import Updater

from .handlers import handlers


class Bot(object):

    @classmethod
    def init(cls):
        if not hasattr(cls, '_bot'):
            cls.updater = Updater(token=settings.TELEGRAM_TOKEN)
            cls.dispatcher = cls.updater.dispatcher
            for handler in handlers:
                cls.dispatcher.add_handler(handler)

            cls.updater.start_polling()
