# coding: utf-8
from __future__ import (
    absolute_import, division, print_function, unicode_literals
)

from datetime import datetime

import telegram
from telegram.ext import CommandHandler, MessageHandler, Filters

from botpyproto.models import TelegramUser

__all__ = ['handlers']


COMEBACK_MESSAGE = 'I want to come back!'

CURRENT_TIME = 'What is the current time?'
STATIC_MESSAGE = 'Static message.'
REFUSAL_OF_USE = 'Refusal of use.'

BREAKUP_MESSAGE = ('I just...I just woke up one day and I knew...'
                   ' what I was never sure of with you.')

OK_KEYBOARD_MARKUP = telegram.ReplyKeyboardMarkup([
    [CURRENT_TIME, STATIC_MESSAGE],
    [REFUSAL_OF_USE]
])
REFUSED_KEYBOARD_MARKUP = telegram.ReplyKeyboardMarkup([
    [COMEBACK_MESSAGE]
])


def start(bot, update):
    """
    Bot start function. Called when the user starts the bot.

    Args:
        bot (telegram.Bot): Bot instance.
        update (telegram.Update): Received update.

    """
    telegram_user = update.message.from_user
    data = {
        'id': telegram_user.id,
        'first_name': telegram_user.first_name,
        'last_name': telegram_user.last_name,
        'username': telegram_user.username
    }
    user, created = TelegramUser.objects.get_or_create(data)
    name = telegram_user.first_name or telegram_user.username

    if user.refused:
        message = (
            'Hello, {}. Unfortunately, you refused to use me. :('.format(name))
        keyboard_markup = REFUSED_KEYBOARD_MARKUP
    else:
        message = 'Hello, {}. How can I help you today?'.format(name)
        keyboard_markup = OK_KEYBOARD_MARKUP

    bot.send_message(
        chat_id=update.message.chat_id, text=message,
        reply_markup=keyboard_markup)


def message_handler(bot, update):
    telegram_user = update.message.from_user
    # in case user will send a message before /start saves him
    try:
        user = TelegramUser.objects.get(pk=telegram_user.id)
        refused = user.refused
    except TelegramUser.DoesNotExist:
        user = None
        refused = False

    name = telegram_user.first_name or telegram_user.username
    user_message = update.message.text

    if refused:
        keyboard_markup = REFUSED_KEYBOARD_MARKUP
    else:
        keyboard_markup = OK_KEYBOARD_MARKUP

    # some keyboard message dispatcher would be nice
    if refused:
        if user_message == COMEBACK_MESSAGE:
            message = 'Welcome back, {}!'.format(name)
            keyboard_markup = OK_KEYBOARD_MARKUP
            user.refused = False
            user.save()
        else:
            message = (
                'Hello, {}. Unfortunately, you refused to use me. :('.format(
                    name))
    elif user_message == CURRENT_TIME:
        message = (
            'The current UTC time is {}'.format(datetime.utcnow().ctime()))
    elif user_message == STATIC_MESSAGE:
        message = 'You are fabulous!'
    elif user_message == REFUSAL_OF_USE:
        message = BREAKUP_MESSAGE
        keyboard_markup = REFUSED_KEYBOARD_MARKUP
        if user:
            user.refused = True
            user.save()
    else:
        message = 'I don\'t understand what you are talking about.'

    bot.send_message(chat_id=update.message.chat_id, text=message,
                     reply_markup=keyboard_markup)


handlers = [
    CommandHandler('start', start),
    MessageHandler(filters=Filters.text, callback=message_handler)
]
