# coding: utf-8
from __future__ import (
    absolute_import, division, print_function, unicode_literals
)
"""
WSGI config for botpyproto project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.11/howto/deployment/wsgi/
"""

import os

from django.core.wsgi import get_wsgi_application

# maybe it can be moved to management command of AppConfig class
from botpyproto.bot import Bot

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "botpyproto.settings")

application = get_wsgi_application()

Bot.init()
